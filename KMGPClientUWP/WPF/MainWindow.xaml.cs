﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.WPF;

namespace WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : FormsApplicationPage
    {
        public MainWindow()
        {
            InitializeComponent(); 
            Forms.Init();
            LoadApplication(new KMGPClientUWP.App());
        }
    }
}
