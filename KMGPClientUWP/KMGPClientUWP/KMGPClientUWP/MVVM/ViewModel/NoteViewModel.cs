﻿using KMGPClientUWP.MVVM.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace KMGPClientUWP.MVVM.ViewModel
{
    public class NoteViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<Note> _notes;
        private Note _selectedNote = new Note();
        private Note _note = new Note();
        private DateTime _date1 = new DateTime();
        private DateTime _date2 = new DateTime();
        private int? _id;
        private int _count;
        private ICommand _edit;
        private ICommand _add;
        private ICommand _search;
        private ICommand _delete;
        private ICommand _searhDate;
        private ICommand _deleteList;
        private ICommand _allList;
        HttpClient client = new HttpClient();
        HttpRequestMessage request = new HttpRequestMessage();
        public ObservableCollection<Note> Notes
        {
            get => _notes; set
            {
                _notes = value;
                OnPropertyChanged();
            }
        }

        public Note SelectedNote
        {
            get => _selectedNote;
            set
            {
                _selectedNote = value;
                OnPropertyChanged();
            }
        }

        public ICommand EditCommand
        {
            get => _edit; set
            {
                _edit = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddCommand
        {
            get => _add;
            set
            {
                _add = value;
                OnPropertyChanged();
            }
        }

        public Note NoteNew
        {
            get => _note;
            set
            {
                _note = value;
                OnPropertyChanged();
            }
        }

        public ICommand SearchCommand
        {
            get => _search;
            set
            {
                _search = value;
                OnPropertyChanged();
            }
        }

        public int? Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        public ICommand DeleteCommand
        {
            get => _delete;
            set
            {
                _delete = value;
                OnPropertyChanged();
            }
        }

        public DateTime Date1
        {
            get => _date1;
            set
            {
                _date1 = value;
                OnPropertyChanged();
            }
        }
        public DateTime Date2
        {
            get => _date2;
            set
            {
                _date2 = value;
                OnPropertyChanged();
            }
        }

        public ICommand SearchDate
        {
            get => _searhDate;
            set
            {
                _searhDate = value;
            }
        }

        public ICommand DeleteListCommand
        {
            get => _deleteList;
            set
            {
                _deleteList = value;
                OnPropertyChanged();
            }
        }
        public ICommand AllListCommand
        {
            get => _allList;
            set
            {
                _allList = value;
                OnPropertyChanged();
            }
        }

        public int Count
        {
            get => _count;
            set
            {
                _count = value;
                OnPropertyChanged();
            }
        }


        public NoteViewModel()
        {
            client.BaseAddress = new Uri("http://localhost:5000/");
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            Notes = new ObservableCollection<Note>();
            Task.Run(async () => await Load());
            EditCommand = new Command(async () => await Edit());
            AddCommand = new Command(async () => await Add());
            SearchCommand = new Command(async () => await Searh());
            DeleteCommand = new Command(async () => await Delete());
            SearchDate = new Command(async () => await Date());
            DeleteListCommand = new Command(async () => await DeleteList());
            AllListCommand = new Command(async () => await AllList());
        }

        private async Task AllList()
        {
            await Load();
        }

        private async Task DeleteList()
        {
            var deleteList = await Date();
            if (deleteList != null)
            {
                foreach (var item in deleteList)
                {
                    HttpRequestMessage request = new HttpRequestMessage();
                    request.Method = HttpMethod.Delete;
                    request.RequestUri = new Uri("http://localhost:5000/api/Note/" + item.Id);
                    request.Headers.Add("Accept", "application/json");
                    var response = await client.DeleteAsync(request.RequestUri);
                }
                await Load();
            }
        }

        private async Task<ObservableCollection<Note>> Date()
        {
            if (_date1 != null && _date2 != null && _date1 < _date2)
            {
                var responseMessage = await client.GetStringAsync("api/Note/List");
                if (!string.IsNullOrEmpty(responseMessage))
                {
                    var notes = new ObservableCollection<Note>();
                    Notes = JsonConvert.DeserializeObject<ObservableCollection<Note>>(responseMessage);
                    foreach (var item in Notes)
                    {
                        if (item.Date > _date1 && item.Date <= _date2)
                        {
                            notes.Add(item);
                        }
                    }
                    Notes = new ObservableCollection<Note>();
                    Notes = notes;
                    Count = Notes.Count;
                    return Notes;
                }
            }
            return null;
        }

        private async Task Delete()
        {
            var value = await Searh();
            if (value != null)
            {
                HttpRequestMessage request = new HttpRequestMessage();
                request.Method = HttpMethod.Delete;
                request.RequestUri = new Uri("http://localhost:5000/api/Note/" + value.Id);
                request.Headers.Add("Accept", "application/json");
                var response = await client.DeleteAsync(request.RequestUri);
                await Load();
            }
        }

        public async Task<Note> Searh()
        {
            if (_id != null)
            {
                HttpRequestMessage request = new HttpRequestMessage();
                request.Method = HttpMethod.Get;
                request.RequestUri = new Uri("http://localhost:5000/api/Note/" + _id);
                request.Headers.Add("Accept", "application/json");
                var response = await client.GetStringAsync(request.RequestUri);
                var result = JsonConvert.DeserializeObject<Note>(response);
                Notes = new ObservableCollection<Note>();
                Notes.Add(result);
                Count = Notes.Count;
                var note = new Note();
                return result;
            }
            return null;
        }

        private async Task Add()
        {
            if (_note.Context != null)
            {
                request.Method = HttpMethod.Post;
                request.RequestUri = new Uri("http://localhost:5000/api/Note/"); 
                _note.Date = DateTime.Now;
                var content = new StringContent(JsonConvert.SerializeObject(_note), Encoding.UTF8, "application/json");
                request.Content = content;
                request.Headers.Add("Accept", "application/json");
                var response = await client.PostAsync(request.RequestUri, request.Content);
                await Load();
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Предупреждение", "Нельзя отправить пустую запись", "Ok");
            }
        }

        private async Task Edit()
        {
            if (_selectedNote != null)
            {
                HttpRequestMessage request = new HttpRequestMessage();
                request.Method = HttpMethod.Put;
                request.RequestUri = new Uri("http://localhost:5000/api/Note/");
                var content = new StringContent(JsonConvert.SerializeObject(_selectedNote), Encoding.UTF8, "application/json");
                request.Content = content;
                request.Headers.Add("Accept", "application/json");
                var response = await client.SendAsync(request);
            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Предупреждение", "Невыбрана запись", "Ok");
            }
        }

        public async Task Load()
        {
            try
            {
                var responseMessage = await client.GetStringAsync("api/Note/List");
                if (!string.IsNullOrEmpty(responseMessage))
                {
                    Notes = JsonConvert.DeserializeObject<ObservableCollection<Note>>(responseMessage);
                    Count = Notes.Count;
                }
                else
                {
                    await Application.Current.MainPage.DisplayAlert("Ошибка", "Ошибка соединения", "Ok");
                }
            }
            catch (Exception ex)
            {

            }
        }



        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}
